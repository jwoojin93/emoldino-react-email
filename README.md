## What to set after building (빌드 후 설정해야 될 내용)

# EN

- Please change 37.5em to 37.5rem with full search (to prevent the width from being different for each email template.)
- /static/images => /images (Please change the image address. The image path is different for the react email project and mms.)
- Please refer to backup_out_comment for comments.

# KR

- 전체검색으로 37.5em 을 37.5rem 으로 변경해주세요 (이메일 템플릿마다 넓이가 상이해지는걸 방지합니다.)
- /static/images => /images (이미지 주소를 변경해주세요 react email 프로젝트와 mms 는 이미지 경로가 다릅니다.)
- 주석은 backup_out_comment 내용을 참고해주세요.
