import React from "react";
import {
  Tailwind,
  Preview,
  Html,
  Head,
  Body,
  Container,
  Row,
  Column,
} from "@react-email/components";

// emoldino component
import EmdnSection from "../components/EmdnSection";
import EmdnHeading from "../components/EmdnHeading";
import StatusCircle from "../components/StatusCircle";
import EmdnButton from "../components/EmdnButton";

// emoldino image
import EmdnLogoImg from "../components/image/EmdnLogoImg";
import EmailInnerImg from "../components/image/EmailInnerImg";
// emoldino table
import TableRow from "../components/table/TableRow";
import TableTh from "../components/table/TableTh";
import TableTd from "../components/table/TableTd";
import EmdnDescription from "../components/EmdnDescription";
import EmdnComment from "../components/EmdnComment";

import {
  dataRequestResgisterationDescription,
  dataRequestCompletionDescription,
} from "../static/js/description";

function EmoldinoNotificationEmail() {
  let dataRequestState = ""; // "completion" || "registeration"
  return (
    <Html>
      <Head />
      <Preview>Notification from eMoldino</Preview>
      <Tailwind>
        <Body>
          <Container className="bg-white p-10 shadow-sm">
            {/* emoldino logo */}
            <EmdnSection>
              <EmdnLogoImg />
            </EmdnSection>
            {/* data-request image */}
            <EmdnSection>
              <EmailInnerImg
                src="/static/images/email-template/data-request.svg"
                alt="data request image"
              />
            </EmdnSection>
            {/* heading */}
            <EmdnSection>
              <EmdnHeading>
                Emma Ko (eMoldino) has requested a Data{" "}
                {dataRequestState === "registeration" && "Registration"}
                {dataRequestState === "completion" && "Completion"}
                {dataRequestState === "" && "Registration Completion"}
              </EmdnHeading>
            </EmdnSection>
            {/* table */}
            <EmdnSection>
              <TableRow>
                <TableTh>Data type</TableTh>
                <TableTh>Data Request</TableTh>
                <TableTh>Due Date</TableTh>
              </TableRow>
              <TableRow style={{ borderTop: "0px", borderBottom: "0px" }}>
                <TableTd>Tooling</TableTd>
                <TableTd>data request value</TableTd>
                <TableTd>due date value</TableTd>
              </TableRow>
              <TableRow style={{ borderTop: "0px", borderBottom: "0px" }}>
                <TableTd>Part</TableTd>
                <TableTd>data request value</TableTd>
                <TableTd>due date value</TableTd>
              </TableRow>
              <TableRow style={{ borderTop: "0px", borderBottom: "0px" }}>
                <TableTd>Machine</TableTd>
                <TableTd>data request value</TableTd>
                <TableTd>due date value</TableTd>
              </TableRow>
              <TableRow style={{ borderTop: "0px", borderBottom: "0px" }}>
                <TableTd>Company</TableTd>
                <TableTd>data request value</TableTd>
                <TableTd>due date value</TableTd>
              </TableRow>
              <TableRow style={{ borderTop: "0px", borderBottom: "0px" }}>
                <TableTd>Plant</TableTd>
                <TableTd>data request value</TableTd>
                <TableTd>due date value</TableTd>
              </TableRow>
              <TableRow style={{ borderTop: "0px" }}>
                <TableTd>Category</TableTd>
                <TableTd>data request value</TableTd>
                <TableTd>due date value</TableTd>
              </TableRow>
            </EmdnSection>
            {/* completion comment */}
            {dataRequestState === "completion" && (
              <EmdnSection>
                <EmdnComment>
                  <Row style={{ fontWeight: 700 }}>
                    <Column style={{ width: "20px" }}>
                      <StatusCircle
                        style={{
                          width: "20px",
                          height: "20px",
                          background: "#FFACAC",
                        }}
                      >
                        CY
                      </StatusCircle>
                    </Column>
                    <Column style={{ paddingLeft: "8px" }}>
                      <strong>Chris Yeung (eMoldino)</strong>
                    </Column>
                  </Row>
                  <Row style={{ marginTop: "5px" }}>
                    <Column>
                      I have added a number of areas of the eMoldino system
                      where we are lacking in data. It would be great if you
                      could follow up this to maximize the data we have in the
                      system....
                    </Column>
                  </Row>
                </EmdnComment>
              </EmdnSection>
            )}
            {/* description */}
            <EmdnSection>
              {(dataRequestState === "completion" ||
                dataRequestState === "") && (
                <EmdnDescription
                  style={{ fontSize: "11.25px" }}
                  dangerouslySetInnerHTML={{
                    __html: dataRequestCompletionDescription,
                  }}
                ></EmdnDescription>
              )}
              {(dataRequestState === "registeration" ||
                dataRequestState === "") && (
                <EmdnDescription
                  style={{ fontSize: "11.25px" }}
                  dangerouslySetInnerHTML={{
                    __html: dataRequestResgisterationDescription,
                  }}
                ></EmdnDescription>
              )}
            </EmdnSection>
            {/* button */}
            <EmdnSection style={{ textAlign: "center" }}>
              <EmdnButton>Log In and View Details</EmdnButton>
            </EmdnSection>
          </Container>
        </Body>
      </Tailwind>
    </Html>
  );
}

export default EmoldinoNotificationEmail;
