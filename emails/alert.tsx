import React from "react";
import {
  Tailwind,
  Preview,
  Html,
  Head,
  Body,
  Container,
  Row,
  Column,
} from "@react-email/components";

// emoldino component
import EmdnSection from "../components/EmdnSection";
import EmdnHeading from "../components/EmdnHeading";
import StatusCircle from "../components/StatusCircle";
import EmdnButton from "../components/EmdnButton";

// emoldino image
import EmdnLogoImg from "../components/image/EmdnLogoImg";
import EmailInnerImg from "../components/image/EmailInnerImg";
// emoldino table
import TableRow from "../components/table/TableRow";
import TableTh from "../components/table/TableTh";
import TableTd from "../components/table/TableTd";
import EmdnDescription from "../components/EmdnDescription";
import EmdnComment from "../components/EmdnComment";
import TableDescription from "../components/table/TableDescription";

import { alertMachineDowntimeRejectionDescription } from "../static/js/description";

function EmoldinoNotificationEmail() {
  let alertState = ""; // "disconnection" || "reset" || "downtime-rejection" || "downtime-created" || "unmatched-sensor" || "temperature"
  return (
    <Html>
      <Head />
      <Preview>Notification from eMoldino</Preview>
      <Tailwind>
        <Body>
          <Container className="bg-white p-10 shadow-sm">
            {/* emoldino logo */}
            <EmdnSection>
              <EmdnLogoImg />
            </EmdnSection>
            {/* icon image */}
            <EmdnSection>
              {(alertState === "disconnection" || alertState === "") && (
                <EmailInnerImg
                  src="/static/images/email-template/disconnection.svg"
                  alt="disconnection image"
                />
              )}
              {(alertState === "reset" || alertState === "") && (
                <EmailInnerImg
                  src="/static/images/email-template/reset.svg"
                  alt="reset image"
                />
              )}
              {(alertState === "downtime-rejection" ||
                alertState === "downtime-created" ||
                alertState === "") && (
                <EmailInnerImg
                  src="/static/images/email-template/machine-downtime.svg"
                  alt="machine downtime image"
                />
              )}
              {(alertState === "unmatched-sensor" || alertState === "") && (
                <EmailInnerImg
                  src="/static/images/email-template/unmatched.svg"
                  alt="unmatched image"
                />
              )}
              {(alertState === "temperature" || alertState === "") && (
                <EmailInnerImg
                  src="/static/images/email-template/temperature.svg"
                  alt="temperature image"
                />
              )}
            </EmdnSection>
            {/* heading */}
            <EmdnSection>
              <EmdnHeading>
                {/* alert / disconnection */}
                {(alertState === "disconnection" || alertState === "") &&
                  "1 tooling is currently disconnected"}
                {/* alert / reset */}
                {(alertState === "reset" || alertState === "") &&
                  "1 tooling is requesting reset"}
                {/* alert / downtime-rejection */}
                {(alertState === "downtime-rejection" || alertState === "") &&
                  "Chris Yeung (eMoldino) has rejected your Machine Downtime Reasons"}
                {/* alert / downtime-created */}
                {(alertState === "downtime-created" || alertState === "") &&
                  "Machine M3764987 Downtime Alert has been created"}
                {/* alert / unmatched-sensor */}
                {(alertState === "unmatched-sensor" || alertState === "") &&
                  "Tooling with sensor A123456789 is in production but unmatched on the system"}
                {/* alert /temperature */}
                {(alertState === "temperature" || alertState === "") &&
                  "Summary of Temperature Alerts for May 21, 2023"}
              </EmdnHeading>
            </EmdnSection>
            {/* disconnection table */}
            {(alertState === "disconnection" || alertState === "") && (
              <EmdnSection>
                <TableRow>
                  <TableTh>Tooling ID</TableTh>
                  <TableTh>Company</TableTh>
                  <TableTh>Plant</TableTh>
                  <TableTh>Last Connection</TableTh>
                </TableRow>
                <TableRow style={{ borderTop: "0px" }}>
                  <TableTd>39048</TableTd>
                  <TableTd>Flextronics</TableTd>
                  <TableTd>BAE UK</TableTd>
                  <TableTd>2021-11-08 18:03:45</TableTd>
                </TableRow>
              </EmdnSection>
            )}
            {/* reset table */}
            {(alertState === "reset" || alertState === "") && (
              <EmdnSection>
                <TableRow>
                  <TableTh>Tooling ID</TableTh>
                  <TableTh>Company</TableTh>
                  <TableTh>Plant</TableTh>
                  <TableTh>Reset Value</TableTh>
                </TableRow>
                <TableRow style={{ borderTop: "0px" }}>
                  <TableTd>39048</TableTd>
                  <TableTd>Flextronics</TableTd>
                  <TableTd>BAE UK</TableTd>
                  <TableTd>100</TableTd>
                </TableRow>
              </EmdnSection>
            )}
            {/* downtime-rejection table */}
            {(alertState === "downtime-rejection" || alertState === "") && (
              <EmdnSection>
                <TableRow>
                  <TableTh>Machine ID</TableTh>
                  <TableTh>Company</TableTh>
                  <TableTh>Plant</TableTh>
                  <TableTh>Date</TableTh>
                </TableRow>
                <TableRow style={{ borderTop: "0px" }}>
                  <TableTd>M37644987</TableTd>
                  <TableTd>Flextronics</TableTd>
                  <TableTd>BAE UK</TableTd>
                  <TableTd>2022-02-01</TableTd>
                </TableRow>
              </EmdnSection>
            )}
            {/* downtime-created table */}
            {(alertState === "downtime-created" || alertState === "") && (
              <EmdnSection>
                <TableRow>
                  <TableTh>Machine ID</TableTh>
                  <TableTh>Company</TableTh>
                  <TableTh>Plant</TableTh>
                  <TableTh>Date</TableTh>
                </TableRow>
                <TableRow style={{ borderTop: "0px" }}>
                  <TableTd>39048</TableTd>
                  <TableTd>Flextronics</TableTd>
                  <TableTd>BAE UK</TableTd>
                  <TableTd>2022-02-01</TableTd>
                </TableRow>
              </EmdnSection>
            )}
            {/* unmatched-sensor table */}
            {(alertState === "unmatched-sensor" || alertState === "") && (
              <EmdnSection>
                <TableRow>
                  <TableTh>Sensor ID</TableTh>
                  <TableTh>Plant</TableTh>
                  <TableTh>Shot Number</TableTh>
                  <TableTh>Tooling Status</TableTh>
                </TableRow>
                <TableRow style={{ borderTop: "0px" }}>
                  <TableTd>39048</TableTd>
                  <TableTd>Flextronics</TableTd>
                  <TableTd>BAE UK</TableTd>
                  <TableTd>
                    <Row>
                      <Column>
                        <StatusCircle />
                      </Column>
                      <Column style={{ paddingLeft: "4px" }}>Outside L1</Column>
                    </Row>
                  </TableTd>
                </TableRow>
              </EmdnSection>
            )}
            {/* temperature table */}
            {(alertState === "temperature" || alertState === "") && (
              <EmdnSection>
                <TableRow>
                  <TableTh>Tooling ID</TableTh>
                  <TableTh>Company</TableTh>
                  <TableTh>Plant</TableTh>
                  <TableTh>Temperature Status</TableTh>
                </TableRow>
                <TableRow style={{ borderTop: "0px" }}>
                  <TableTd>39048</TableTd>
                  <TableTd>Flextronics</TableTd>
                  <TableTd>BAE UK</TableTd>
                  <TableTd>
                    <Row>
                      <Column>
                        <StatusCircle />
                      </Column>
                      <Column style={{ paddingLeft: "4px" }}>Outside L1</Column>
                    </Row>
                  </TableTd>
                </TableRow>
              </EmdnSection>
            )}
            {/* temperature table description */}
            {(alertState === "temperature" || alertState === "") && (
              <TableDescription>
                Tooling T123456789 was outside the temperature tolerance range
              </TableDescription>
            )}
            {/* downtime-rejection comment */}
            {(alertState === "downtime-rejection" || alertState === "") && (
              <EmdnSection>
                <EmdnComment>
                  <Row style={{ fontWeight: 700 }}>
                    <Column style={{ width: "20px" }}>
                      <StatusCircle
                        style={{
                          width: "20px",
                          height: "20px",
                          background: "#FFACAC",
                        }}
                      >
                        CY
                      </StatusCircle>
                    </Column>
                    <Column style={{ paddingLeft: "8px" }}>
                      <strong>Chris Yeung (eMoldino)</strong>
                    </Column>
                  </Row>
                  <Row style={{ marginTop: "5px" }}>
                    <Column>
                      I have added a number of areas of the eMoldino system
                      where we are lacking in data. It would be great if you
                      could follow up this to maximize the data we have in the
                      system....
                    </Column>
                  </Row>
                </EmdnComment>
              </EmdnSection>
            )}
            {/* downtime-rejection description */}
            {(alertState === "downtime-rejection" || alertState === "") && (
              <EmdnSection>
                <EmdnDescription
                  style={{ fontSize: "11.25px" }}
                  dangerouslySetInnerHTML={{
                    __html: alertMachineDowntimeRejectionDescription,
                  }}
                ></EmdnDescription>
              </EmdnSection>
            )}
            {/* emodino button */}
            <EmdnSection style={{ textAlign: "center" }}>
              <EmdnButton>
                {/* alert / disconnection */}
                {/* alert / reset */}
                {(alertState === "disconnection" ||
                  alertState === "reset" ||
                  alertState === "") &&
                  "Log In and View Details"}

                {/* alert / downtime-rejection */}
                {(alertState === "downtime-rejection" || alertState === "") &&
                  "Log In and Edit Downtime Alert Registration"}

                {/* alert / downtime-created */}
                {/* alert / unmatched-sensor */}
                {/* alert / temperature */}
                {(alertState === "downtime-created" ||
                  alertState === "unmatched-sensor" ||
                  alertState === "temperature" ||
                  alertState === "") &&
                  "Log In and View Details"}
              </EmdnButton>
            </EmdnSection>
          </Container>
        </Body>
      </Tailwind>
    </Html>
  );
}

export default EmoldinoNotificationEmail;
