import React from "react";
import {
  Tailwind,
  Preview,
  Html,
  Head,
  Body,
  Container,
} from "@react-email/components";

// emoldino component
import EmdnSection from "../components/EmdnSection";
import EmdnHeading from "../components/EmdnHeading";
import EmdnButton from "../components/EmdnButton";

// emoldino image
import EmdnLogoImg from "../components/image/EmdnLogoImg";
import EmailInnerImg from "../components/image/EmailInnerImg";
// emoldino table
import TableRow from "../components/table/TableRow";
import TableTh from "../components/table/TableTh";
import TableTd from "../components/table/TableTd";

function EmoldinoNotificationEmail() {
  let detailState = ""; // "approved" || "disapproved" || "waiting-approval"

  return (
    <Html>
      <Head />
      <Preview>Notification from eMoldino</Preview>
      <Tailwind>
        <Body>
          <Container className="bg-white p-10 shadow-sm">
            {/* emoldino logo */}
            <EmdnSection>
              <EmdnLogoImg />
            </EmdnSection>
            {/* icon image */}
            <EmdnSection>
              <EmailInnerImg
                src="/static/images/email-template/tooling.svg"
                alt="tooling image"
              />
            </EmdnSection>
            {/* heading */}
            <EmdnSection>
              <EmdnHeading>
                {/* tooling / approved */}
                {(detailState === "approved" || detailState === "") &&
                  "Tooling 390480 has been approved"}
                {/* tooling / disapproved */}
                {(detailState === "disapproved" || detailState === "") &&
                  "Tooling 390480 has been disapproved"}
                {/* tooling / waiting-approval */}
                {(detailState === "waiting-approval" || detailState === "") &&
                  "A tooling is waiting for approval"}
              </EmdnHeading>
            </EmdnSection>
            {/* emoldino table */}
            <EmdnSection>
              {/* tooling / approved */}
              {/* tooling / disapproved */}
              {/* tooling / waiting-approval */}
              <TableRow>
                <TableTh>Tooling ID</TableTh>
                <TableTh>Type</TableTh>
                <TableTh>Plant</TableTh>
              </TableRow>
              <TableRow style={{ borderTop: "0px" }}>
                <TableTd>tooling id value</TableTd>
                <TableTd>type value</TableTd>
                <TableTd>plant value</TableTd>
              </TableRow>
            </EmdnSection>
            {/* emodino button */}
            <EmdnSection style={{ textAlign: "center" }}>
              <EmdnButton>
                {/* tooling / approved */}
                {/* tooling / disapproved */}
                {/* tooling / waiting for approval) */}
                Log In and View Details
              </EmdnButton>
            </EmdnSection>
          </Container>
        </Body>
      </Tailwind>
    </Html>
  );
}

export default EmoldinoNotificationEmail;
