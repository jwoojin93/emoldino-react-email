import React from "react";
import {
  Tailwind,
  Preview,
  Html,
  Head,
  Body,
  Container,
} from "@react-email/components";

// emoldino component
import EmdnSection from "../components/EmdnSection";
import EmdnHeading from "../components/EmdnHeading";
import EmdnButton from "../components/EmdnButton";

// emoldino image
import EmdnLogoImg from "../components/image/EmdnLogoImg";
import EmailInnerImg from "../components/image/EmailInnerImg";
// emoldino table
import TableRow from "../components/table/TableRow";
import TableTh from "../components/table/TableTh";
import TableTd from "../components/table/TableTd";
import EmdnDescription from "../components/EmdnDescription";

import { systemInviteDescription } from "../static/js/description";

function EmoldinoNotificationEmail() {
  let systemState = ""; // "access-approved" || "new-user-requesting-access" || "invite-first" || "invite-reminder"

  return (
    <Html>
      <Head />
      <Preview>Notification from eMoldino</Preview>
      <Tailwind>
        <Body>
          <Container className="bg-white p-10 shadow-sm">
            {/* emoldino logo */}
            <EmdnSection>
              <EmdnLogoImg />
            </EmdnSection>
            {/* icon image */}
            <EmdnSection>
              {(systemState === "access-approved" || systemState === "") && (
                <EmailInnerImg
                  src="/static/images/email-template/checkmark.svg"
                  alt="checkmark image"
                />
              )}
              {(systemState === "new-user-requesting-access" ||
                systemState === "") && (
                <EmailInnerImg
                  src="/static/images/email-template/lock.svg"
                  alt="lock image"
                />
              )}
              {(systemState === "invite-first" ||
                systemState === "invite-reminder" ||
                systemState === "") && (
                <EmailInnerImg
                  src="/static/images/email-template/invitation.svg"
                  alt="invitation image"
                />
              )}
            </EmdnSection>
            {/* heading */}
            <EmdnSection>
              <EmdnHeading>
                {(systemState === "access-approved" || systemState === "") &&
                  "Your access request has been approved"}
                {(systemState === "new-user-requesting-access" ||
                  systemState === "") &&
                  "You have a new user requesting access to the system"}
                {(systemState === "invite-first" || systemState === "") &&
                  "You are invited!"}
                {(systemState === "invite-reminder" || systemState === "") &&
                  "It looks like you haven’t joined!"}
              </EmdnHeading>
            </EmdnSection>
            {/* table */}
            {(systemState === "new-user-requesting-access" ||
              systemState === "") && (
              <EmdnSection>
                <TableRow>
                  <TableTh>Name</TableTh>
                  <TableTh>Email</TableTh>
                  <TableTh>Company</TableTh>
                  <TableTh>Position</TableTh>
                </TableRow>
                <TableRow style={{ borderTop: "0px" }}>
                  <TableTd>Emma Ko</TableTd>
                  <TableTd
                    style={{ wordBreak: "break-word", fontSize: "10px" }}
                  >
                    <a href="www.google.com">emmako@gmail.com</a>
                  </TableTd>
                  <TableTd>Flextionics</TableTd>
                  <TableTd>Product Designer</TableTd>
                </TableRow>
              </EmdnSection>
            )}
            {/* description */}
            {(systemState === "invite-first" ||
              systemState === "invite-reminder" ||
              systemState === "") && (
              <EmdnSection>
                <EmdnDescription
                  style={{ fontSize: "11.25px" }}
                  dangerouslySetInnerHTML={{ __html: systemInviteDescription }}
                ></EmdnDescription>
              </EmdnSection>
            )}
            {/* button */}
            <EmdnSection style={{ textAlign: "center" }}>
              <EmdnButton>
                {(systemState === "access-approved" || systemState === "") &&
                  "Log In Now"}
                {(systemState === "new-user-requesting-access" ||
                  systemState === "") &&
                  "Log In and View Details"}
                {(systemState === "invite-first" ||
                  systemState === "invite-reminder" ||
                  systemState === "") &&
                  "Request Access"}
              </EmdnButton>
            </EmdnSection>
          </Container>
        </Body>
      </Tailwind>
    </Html>
  );
}

export default EmoldinoNotificationEmail;
