import React from "react";
import {
  Tailwind,
  Preview,
  Html,
  Head,
  Body,
  Container,
} from "@react-email/components";

// emoldino component
import EmdnSection from "../components/EmdnSection";
import EmdnHeading from "../components/EmdnHeading";
import EmdnButton from "../components/EmdnButton";

// emoldino image
import EmdnLogoImg from "../components/image/EmdnLogoImg";
import EmailInnerImg from "../components/image/EmailInnerImg";
// emoldino table
import TableRow from "../components/table/TableRow";
import TableTh from "../components/table/TableTh";
import TableTd from "../components/table/TableTd";

function EmoldinoNotificationEmail() {
  let workorderState = ""; // acceptance || approval || request-declined || overdue || request-maintenance || request-refurbishment-only
  return (
    <Html>
      <Head />
      <Preview>Notification from eMoldino</Preview>
      <Tailwind>
        <Body>
          <Container className="bg-white p-10 shadow-sm">
            {/* emoldino logo */}
            <EmdnSection>
              <EmdnLogoImg />
            </EmdnSection>
            {/* icon image */}
            <EmdnSection>
              <EmailInnerImg
                src="/static/images/email-template/work-order.svg"
                alt="work order image"
              />
            </EmdnSection>
            {/* heading */}
            <EmdnSection>
              <EmdnHeading>
                {/* acceptance heading */}
                {(workorderState === "acceptance" || workorderState === "") &&
                  "You have been assigned a Work Order by Emma Ko (eMoldino)."}
                {/* approval heading */}
                {(workorderState === "approval" || workorderState === "") &&
                  "Edit request is sent to you by Emma Ko (eMoldino)"}
                {/* request-declined heading */}
                {(workorderState === "request-declined" ||
                  workorderState === "") &&
                  "Your Work Order ID WO-0000087 request has been declined."}
                {/* overdue heading */}
                {(workorderState === "overdue" || workorderState === "") &&
                  "Work order ID WO-0000087 has not yet been completed and is overdue."}
                {/* request-maintenance heading */}
                {(workorderState === "request-maintenance" ||
                  workorderState === "") &&
                  "Approval request to carry out corrective maintenance."}
                {/* request-refurbishment-only heading */}
                {(workorderState === "request-refurbishment-only" ||
                  workorderState === "") &&
                  "Approval request for reported work order."}
              </EmdnHeading>
            </EmdnSection>
            {/* table */}
            <EmdnSection>
              <TableRow>
                <TableTh>Work Order ID</TableTh>
                <TableTh>Type</TableTh>
                <TableTh>Priority</TableTh>
                <TableTh>Due Date</TableTh>
              </TableRow>
              <TableRow style={{ borderTop: "0px" }}>
                <TableTd>39048</TableTd>
                <TableTd>Flextronics</TableTd>
                <TableTd></TableTd>
                <TableTd></TableTd>
              </TableRow>
            </EmdnSection>
            {/* button */}
            <EmdnSection style={{ textAlign: "center" }}>
              <EmdnButton>Log In and View Details</EmdnButton>
            </EmdnSection>
          </Container>
        </Body>
      </Tailwind>
    </Html>
  );
}

export default EmoldinoNotificationEmail;
