import React from "react";
import {
  Tailwind,
  Preview,
  Html,
  Head,
  Body,
  Container,
  Row,
  Column,
} from "@react-email/components";

// emoldino component
import EmdnSection from "../components/EmdnSection";
import EmdnHeading from "../components/EmdnHeading";
import StatusCircle from "../components/StatusCircle";
import EmdnButton from "../components/EmdnButton";

// emoldino image
import EmdnLogoImg from "../components/image/EmdnLogoImg";
import EmailInnerImg from "../components/image/EmailInnerImg";
// emoldino table
import TableRow from "../components/table/TableRow";
import TableTh from "../components/table/TableTh";
import TableTd from "../components/table/TableTd";
import TableDescription from "../components/table/TableDescription";

function EmoldinoNotificationEmail() {
  let alertState = ["relocation", "cycle-time", "uptime"]; // "relocation" || "cycle-time" || "uptime"
  return (
    <Html>
      <Head />
      <Preview>Notification from eMoldino</Preview>
      <Tailwind>
        <Body>
          <Container className="bg-white p-10 shadow-sm">
            {/* emoldino logo */}
            <EmdnSection>
              <EmdnLogoImg />
            </EmdnSection>
            {/* alert image */}
            <EmdnSection>
              <EmailInnerImg
                src="/static/images/email-template/alert.svg"
                alt="alert image"
              />
            </EmdnSection>
            {/* relocation area */}
            {alertState.includes("relocation") && (
              <React.Fragment>
                {/* relocation heading */}
                <EmdnSection>
                  <EmdnHeading>
                    Summary of Relocation Alerts for May 21, 2023
                  </EmdnHeading>
                </EmdnSection>
                {/* relocation table */}
                <EmdnSection>
                  <TableRow>
                    <TableTh>Tooling ID</TableTh>
                    <TableTh>Company</TableTh>
                    <TableTh>Plant</TableTh>
                    <TableTh>status</TableTh>
                  </TableRow>
                  {/* 반복될 수 있음 (최대 3줄) */}
                  <TableRow style={{ borderTop: "0px", borderBottom: "0px" }}>
                    <TableTd>39048</TableTd>
                    <TableTd>Flextronics</TableTd>
                    <TableTd>BAE UK</TableTd>
                    <TableTd>
                      <Row>
                        <Column>
                          <StatusCircle />
                        </Column>
                        <Column style={{ paddingLeft: "4px" }}>
                          Outside L1
                        </Column>
                      </Row>
                    </TableTd>
                  </TableRow>
                  <TableRow style={{ borderTop: "0px" }}>
                    <TableTd>39048</TableTd>
                    <TableTd>Flextronics</TableTd>
                    <TableTd>BAE UK</TableTd>
                    <TableTd>
                      <Row>
                        <Column>
                          <StatusCircle />
                        </Column>
                        <Column style={{ paddingLeft: "4px" }}>
                          Outside L1
                        </Column>
                      </Row>
                    </TableTd>
                  </TableRow>
                </EmdnSection>
                {/* relocation button */}
                <EmdnSection style={{ textAlign: "center" }}>
                  <EmdnButton>Log In and View Details</EmdnButton>
                </EmdnSection>
              </React.Fragment>
            )}
            {/* cycle-time area */}
            {alertState.includes("cycle-time") && (
              <React.Fragment>
                {/* cycle-time heading */}
                <EmdnSection>
                  <EmdnHeading>
                    Summary of CT Alerts for May 21, 2023
                  </EmdnHeading>
                </EmdnSection>
                {/* cycle-time table */}
                <EmdnSection>
                  <TableRow>
                    <TableTh>Tooling ID</TableTh>
                    <TableTh>Company</TableTh>
                    <TableTh>Plant</TableTh>
                    <TableTh>CT Status</TableTh>
                  </TableRow>
                  {/* 반복될 수 있음 (최대 3줄) */}
                  <TableRow style={{ borderTop: "0px", borderBottom: "0px" }}>
                    <TableTd>39048</TableTd>
                    <TableTd>Flextronics</TableTd>
                    <TableTd>BAE UK</TableTd>
                    <TableTd>
                      <Row>
                        <Column>
                          <StatusCircle />
                        </Column>
                        <Column style={{ paddingLeft: "4px" }}>
                          Outside L1
                        </Column>
                      </Row>
                    </TableTd>
                  </TableRow>
                  <TableRow style={{ borderTop: "0px" }}>
                    <TableTd>39048</TableTd>
                    <TableTd>Flextronics</TableTd>
                    <TableTd>BAE UK</TableTd>
                    <TableTd>
                      <Row>
                        <Column>
                          <StatusCircle />
                        </Column>
                        <Column style={{ paddingLeft: "4px" }}>
                          Outside L1
                        </Column>
                      </Row>
                    </TableTd>
                  </TableRow>
                  {/* table description */}
                  <TableDescription>
                    In total, 1 tooling[s] [was / were] beyond the cycle time
                    limits. 2
                  </TableDescription>
                </EmdnSection>
                {/* cycle-time button */}
                <EmdnSection style={{ textAlign: "center" }}>
                  <EmdnButton>Log In and View Details</EmdnButton>
                </EmdnSection>
              </React.Fragment>
            )}
            {/* uptime area */}
            {alertState.includes("uptime") && (
              <React.Fragment>
                {/* uptime heading */}
                <EmdnSection>
                  <EmdnHeading>
                    Summary of Uptime Alerts for May 21, 2023
                  </EmdnHeading>
                </EmdnSection>
                {/* uptime table */}
                <EmdnSection>
                  <TableRow>
                    <TableTh>Tooling ID</TableTh>
                    <TableTh>Company</TableTh>
                    <TableTh>Plant</TableTh>
                    <TableTh>CT Status</TableTh>
                  </TableRow>
                  {/* 반복될 수 있음 (최대 3줄) */}
                  <TableRow style={{ borderTop: "0px", borderBottom: "0px" }}>
                    <TableTd>39048</TableTd>
                    <TableTd>Flextronics</TableTd>
                    <TableTd>BAE UK</TableTd>
                    <TableTd>
                      <Row>
                        <Column>
                          <StatusCircle />
                        </Column>
                        <Column style={{ paddingLeft: "4px" }}>
                          Outside L1
                        </Column>
                      </Row>
                    </TableTd>
                  </TableRow>
                  <TableRow style={{ borderTop: "0px" }}>
                    <TableTd>39048</TableTd>
                    <TableTd>Flextronics</TableTd>
                    <TableTd>BAE UK</TableTd>
                    <TableTd>
                      <Row>
                        <Column>
                          <StatusCircle />
                        </Column>
                        <Column style={{ paddingLeft: "4px" }}>
                          Outside L1
                        </Column>
                      </Row>
                    </TableTd>
                  </TableRow>
                  {/* table description */}
                  <TableDescription>
                    In total, 1 tooling[s] [was / were] beyond efficiency
                    limits.
                  </TableDescription>
                </EmdnSection>
                {/* uptime button */}
                <EmdnSection style={{ textAlign: "center" }}>
                  <EmdnButton>Log In and View Details</EmdnButton>
                </EmdnSection>
              </React.Fragment>
            )}
          </Container>
        </Body>
      </Tailwind>
    </Html>
  );
}

export default EmoldinoNotificationEmail;
