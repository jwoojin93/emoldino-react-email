import React from "react";
import { Img } from "@react-email/components";

const EmailInnerImg = (props) => (
  <Img
    src={props.src}
    alt={props.alt}
    height="60"
    width="60"
    className="mx-auto"
  />
);

export default EmailInnerImg;
