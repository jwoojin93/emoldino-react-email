import React from "react";
import { Img } from "@react-email/components";

const EmdnLogoImg = () => (
  <Img
    src="/static/images/email-template/emoldino-logo.png"
    height="24"
    width="144"
    alt="eMoldino"
    className="mx-auto"
  />
);

export default EmdnLogoImg;
