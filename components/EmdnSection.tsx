import React from "react";
import { Section } from "@react-email/components";

const EmdnSection = (props) => (
  <Section
    style={{
      marginTop: "40px",
      ...props.style,
    }}
  >
    {props.children}
  </Section>
);

export default EmdnSection;
