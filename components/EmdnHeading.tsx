import React from "react";
import { Heading } from "@react-email/components";

const EmdnHeading = (props) => (
  <Heading
    style={{
      textAlign: "center",
      fontSize: "1.5rem",
      lineHeight: "2rem",
      margin: "0",
      ...props.style,
    }}
    // className="text-2xl text-center"
  >
    {props.children}
  </Heading>
);

export default EmdnHeading;
