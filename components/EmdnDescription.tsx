import React from "react";
import { Text } from "@react-email/components";

const EmdnDescription = (props) => (
  <Text
    style={{
      margin: "0 auto",
      width: "80%",
      color: "#4B4B4B",
      fontSize: "14.66px",
      fontFamily: "Helvetica Neue",
      lineHeight: "21px",
      ...props.style,
    }}
    dangerouslySetInnerHTML={props.dangerouslySetInnerHTML}
  >
    {props.children}
  </Text>
);

export default EmdnDescription;
