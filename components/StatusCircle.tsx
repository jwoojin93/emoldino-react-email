import React from "react";
import { Container } from "@react-email/components";

const StatusCircle = (props) => (
  <Container
    style={{
      width: "13px",
      height: "13px",
      borderRadius: "50%",
      background: "#F7CC57",
      fontSize: "9px",
      textAlign: "center",
      ...props.style,
    }}
  >
    {props.children}
  </Container>
);

export default StatusCircle;
