import React from "react";
import { Button } from "@react-email/components";

const EmdnButton = (props) => (
  <Button
    style={{
      borderRadius: "3px",
      background: "#3491FF",
      paddingTop: "6px",
      paddingBottom: "6px",
      paddingLeft: "8px",
      paddingRight: "8px",
      color: "#FFF",
      fontSize: "14.66px",
      cursor: "pointer",
    }}
  >
    {props.children}
  </Button>
);

export default EmdnButton;
