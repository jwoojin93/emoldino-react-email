import React from "react";
import { Row } from "@react-email/components";

const TableRow = (props) => (
  <Row
    style={{
      border: "solid 1px #ddd",
      background: "#F5F8FF",
      tableLayout: "fixed",
      ...props.style,
    }}
  >
    {props.children}
  </Row>
);

export default TableRow;
