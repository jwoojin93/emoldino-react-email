import React from "react";
import { Column } from "@react-email/components";

const TableColumn = (props) => (
  <Column style={{ width: "20rem" }}>{props.children}</Column>
);

export default TableColumn;
