import React from "react";
import { Column } from "@react-email/components";

const TableTd = (props) => (
  <Column
    style={{
      margin: "0",
      color: "#4B4B4B",
      fontSize: "14.66px",
      paddingTop: "8px",
      paddingBottom: "8px",
      paddingLeft: "16px",
      paddingRight: "16px",
      wordBreak: "break-word",
      ...props.style,
    }}
  >
    {props.children}
  </Column>
);

export default TableTd;
