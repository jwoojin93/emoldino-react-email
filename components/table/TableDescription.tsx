import React from "react";
import { Row, Column } from "@react-email/components";

const TableDescription = (props) => (
  <Row>
    <Column
      style={{
        paddingTop: "10px",
        color: "#4B4B4B",
        fontSize: "11.25px",
        fontFamily: "Helvetica Neue",
      }}
    >
      {props.children}
    </Column>
  </Row>
);

export default TableDescription;
