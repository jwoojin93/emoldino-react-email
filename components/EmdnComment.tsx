import React from "react";
import { Container } from "@react-email/components";

const EmdnComment = (props) => (
  <Container
    style={{
      color: "#4B4B4B",
      fontSize: "11.25px",
      fontFamily: "Helvetica Neue",
      borderRadius: "3px",
      border: "0.5px solid rgba(144, 144, 144, 0.00)",
      background: "#E8E8E8",
      padding: "13px 21px 12px 16px",
    }}
  >
    {props.children}
  </Container>
);

export default EmdnComment;
